/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package futoshiki;

import java.io.InputStreamReader;

/**
 *
 * @author sezuka
 */
public class Game {
    protected static final String APP_AUTHOR = "VK";
    protected static final String APP_REVISION = "0.0.4r20";
    protected static final String APP_BUILDMODE = "DEV";
    
    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {
        System.out.println( "Game launching..." );
        
        try {
            MainHandle mh = new MainHandle();
            mh.UserInterface( new InputStreamReader( System.in ) );
        } catch (Exception e) {
            System.err.println( "[ERROR] " + e.toString() );
        } finally {
            System.out.println( "Goodbye!" );
        }
    }
    
}
