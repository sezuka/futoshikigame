/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package futoshiki;

/**
 *
 * @author sezuka
 */
public class SquareNumber {
    private int Value;
    
    public SquareNumber(int sIntValue)
    {
        this.Value = sIntValue;
    }
    
    @Override
    public String toString()
    {
        return (Value > 0) ? Integer.toString(Value) : " ";
    }
    
    public int getValue()
    {
        return Value;
    }
    
    public void setSquare(int pGuessValue)
    {
        this.Value = pGuessValue;
    }
    
}
