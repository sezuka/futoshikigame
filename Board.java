/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package futoshiki;

import java.util.Objects;

/**
 *
 * @author sezuka
 */
public class Board {
    private final SquareNumber[][] Grid;
    private final String[][] rowConstraints;
    private final String[][] columnConstraints;
    
    public Board( int gridSize )
    {
        this.Grid = new SquareNumber[ gridSize ][ gridSize ];
        this.rowConstraints = new String[ gridSize ][ gridSize - 1 ];
        this.columnConstraints = new String[ gridSize - 1 ][ gridSize ];
    }

    @Override
    public String toString()
    {
        String strConstruct = new String(); // Grid assembler String
        
        // START Column Grid Numbers (ColsID)
        strConstruct += "\t"; // Nudge
        for ( int j = 0; j < this.Grid.length; j++ ) // COLID
        {
            strConstruct += " " + (j+1) + "    ";
        }
        strConstruct += "\n";
        // END
            
        for( int i = 0; i < this.Grid.length; i++ ) // LINEFEED
        {
            // START Top parts of Squares
            strConstruct += "\t"; // Nudge
            for ( int j = 0; j < this.Grid.length; j++ ) // UI SQUARE TOP
            {
                strConstruct += "---   ";
            }
            strConstruct += "\n";
            // END
            
            strConstruct += (i+1) + "\t"; // Row Grid Numbers ROWID
            
            // START Number Square Row
            for ( int j = 0; j < this.Grid.length; j++ ) // ROW
            {
                strConstruct += "|" + this.Grid[i][j].toString() + "| "; // Number Square
                if( this.rowConstraints[i].length-1 >= j && this.rowConstraints.length-1 >= i ){ // i(linefeeds) * j-1(innercounter)
                    strConstruct += this.rowConstraints[i][j] + " "; // Row Constraint Square
                }
            }
            strConstruct += "\n"; // Newline (ROW) feed
            // END
            
            // START Bottom parts of Squares
            strConstruct += "\t"; // Nudge
            for ( int j = 0; j < this.Grid.length; j++ ) // UI SQUARE BOTTOM
            {
                strConstruct += "---   ";
            }
            strConstruct += "\n";
            // END
            
            // START Column Constraint + Blank cross-section Row
            strConstruct += "\t"; // Nudge
            if( this.columnConstraints.length-1 >= i ){ // So we do not go out of Array Index
                for ( int j = 0; j < this.Grid.length; j++ ) // COLUMN
                {
                    strConstruct += " " + this.columnConstraints[i][j] + "  "; // Column Constraint Square
                    if( this.columnConstraints[i].length-1 > j && this.columnConstraints.length-1 >= i ){ // i-1(linefeeds) * j(innercounter)
                        strConstruct += "  "; // Blank Space (cross-section) Square
                    }
                }
            }
            strConstruct += "\n"; // Newline (ROW) feed
            // END
        }
        
        return strConstruct; // Return assembled Grid as a String
    }
    
    private void createSquare( int y, int x, int Value )
    {
        this.Grid[y][x] = new SquareNumber( Value );
    }
    
    private void setRowConstraint( int y, int x, String Value )
    {
        this.rowConstraints[y][x] = Value;
    }
    
    private void setColumnConstraint( int y, int x, String Value )
    {
        this.columnConstraints[y][x] = Value;
    }
    
    public void setSquare( int y, int x, int pGuessValue )
    {
        if ( this.Grid == null ){ System.err.println("[GRID] Null Board object supplied to setSquare"); return; }
        
        if( (0 <= y && y <= this.Grid.length) && (0 <= x && x <= this.Grid.length) ) // Check coords
        {
            if( 0 <= pGuessValue && pGuessValue <= this.Grid.length ) // Check user input value
            {
                this.Grid[y][x].setSquare( pGuessValue );
            } else {
                System.err.println( "Please enter a value between 0 and " + this.Grid.length );
            }
        } else {
            System.err.println( "Invalid coordinates" );
        }
    }
    
    public void fillPuzzle()
    {
        if ( this.Grid == null ){ System.err.println("[GRID] Null Board object supplied to fillPuzzle"); return; }
        
        // Fill arrays with empty constraints
        for( int i = 0; i < this.Grid.length; i++)
        {
            for(int j = 0; j < this.Grid.length-1; j++)
            {
                this.setRowConstraint( i, j, " " );
                this.setColumnConstraint(j, i, " ");
            }
        }
        
        // Generate Row constraints
        for( int i = 0; i < (1 + (int)(Math.random() * ((this.Grid.length / 2) + 2)) ); i++ )
            this.setRowConstraint( (int)(Math.random() * (this.rowConstraints.length - 1)), (int)(Math.random() * (this.rowConstraints.length - 2)), ">" );
        //Generate Column constraints
        for( int i = 0; i < (1 + (int)(Math.random() * ((this.Grid.length / 2) + 2)) ); i++ )
            this.setColumnConstraint( (int)(Math.random() * (this.columnConstraints.length - 2)), (int)(Math.random() * (this.columnConstraints.length - 1)), "v" );
        
        // Populate Board with squares
        for(int i = 0; i < this.Grid.length; i++)
        {
            for(int j = 0; j < this.Grid.length; j++)
            {
                this.createSquare( i, j, 0 );
            }
        }
    }
    
    public boolean isLegal()
    {
        if ( this.Grid == null ){ System.err.println("[GRID] Null Board object supplied to isLegal"); return false; }
        
        boolean legalStatus = true;
        int validRowsSum = 0; // Number of valid Rows using sum
        int validColsSum = 0; // Number of valid Cols using sum
        Integer checksum = 0; // Value to check sum against
        Integer rowStorage = 0; // Temp. number storage
        Integer colStorage = 0; // Temp. number storage
        
        
        // Calculate col/row total for verification
        for( int i = 1; i < this.Grid.length+1; i++ )
            checksum += i;
        
        // Check for valid rows/cols via sumation
        for( int i = 0; i < this.Grid.length; i++ )
        {
            for( int j = 0; j < this.Grid.length; j++ )
            {
                rowStorage += this.Grid[i][j].getValue();
                colStorage += this.Grid[j][i].getValue();
            }
            
            if( Objects.equals(rowStorage, checksum) ) validRowsSum++;
            if( Objects.equals(colStorage, checksum) ) validColsSum++;
            rowStorage = 0;
            colStorage = 0;
        }
        
        // Check for correct numbers and reoccurance for LARGE GRIDS > 9
        // i.e. 1, 2, 3, 4, 5 and not 4, 4, 2, 3, 2 as both have checksum = 15
        // At the moment, the isLegal method works as intended
        
        // Check for constraint legality
        for( int i = 0; i < this.Grid.length; i++ )
        {
            for( int j = 0; j < this.Grid.length-1; j++ )
            {
                if( this.rowConstraints[i][j].equals("<") )
                {
                    if( !(this.Grid[i][j].getValue() < this.Grid[i][j+1].getValue()) ) legalStatus = false;
                } else if( this.rowConstraints[i][j].equals(">") ) {
                    if( !(this.Grid[i][j].getValue() > this.Grid[i][j+1].getValue()) ) legalStatus = false;
                }
                
                if( this.columnConstraints[j][i].equals("^") )
                {
                    if( !(this.Grid[j][i].getValue() < this.Grid[j+1][i].getValue()) ) legalStatus = false;
                } else if( this.columnConstraints[j][i].equals("v") ) {
                    if( !(this.Grid[j][i].getValue() > this.Grid[j+1][i].getValue()) ) legalStatus = false;
                }
            }
        }
        
        // Check all conditions are met and return  
        return ( (validColsSum == this.Grid.length) && (validRowsSum == this.Grid[0].length) && (legalStatus != false) );
    }
    
    private String getProblems(){ return "Function Board::getProblems() is not implemented."; }
    
}
