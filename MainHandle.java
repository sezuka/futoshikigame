/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package futoshiki;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author sezuka
 */
public class MainHandle {
    private Board currentGame; // Board Instance (one game)
    
    public MainHandle()
    {
        this.DisplayBanner();
    }
    
    private void Start( int boardSize )
    {
        if( this.currentGame != null ){ System.err.println( "Please finish the current game before starting a new one!" ); return; }
        if( boardSize <= 1 ){ System.err.println( "Please choose a larger board size!" ); return; }
        
        System.out.printf( "[MAIN] Starting a new game...\n" );
        this.currentGame = new Board( boardSize ); // Initialise Board field
        this.currentGame.fillPuzzle();
        if( this.currentGame != null ) System.out.printf( "\t...Complete!\n" );
    }
    
    private void Reset()
    {
        System.out.printf( "[MAIN] Destroying current session...\n" );
        this.currentGame = null;
        if( this.currentGame == null ) System.out.printf( "\t...Complete!\n" );
    }
    
    public String getVersion()
    {
        return "Futoshiki " + Game.APP_REVISION + " (" + Game.APP_BUILDMODE + ")\n";
    }
    
    private void DisplayBanner()
    {
        System.out.printf( "[MAIN] Welcome to " + getVersion() ); // Application Banner
        System.out.printf( "[MAIN] To view the commands available, type 'help'. Type 'exit' to quit.\n" );
    }
    
    private void DisplayMenu()
    {
        System.out.printf( "[MENU] version - Displays the current version of the Game.\n" );
        System.out.printf( "[MENU] help - View this help screen.\n" );
        System.out.printf( "[MENU] exit - Exit this game.\n" );
        System.out.printf( "[MENU] newgame - Start a new game.\n" );
        System.out.printf( "[MENU] legal - Checks if the game is complete.\n" );
        System.out.printf( "[MENU] reset - Resets any current game.\n" );
        System.out.printf( "[MENU] display - Displays the Futoshiki Grid.\n" );
        System.out.printf( "[MENU] setsquare - Sets Square with number using the Y and X coordinates supplied.\n" );
    }
    
    public void UserInterface( InputStreamReader pInput ) throws Exception
    {
        BufferedReader br = new BufferedReader( pInput ); // Player Input Stream from main()
        
        OUTER:
        while( true ) {
            System.out.printf( "> " ); // Display command cursor? (Prompt display)
            String[] args = br.readLine().split( " " ); // Split Player Input into array to allow argument parsing
            String pAction = args[ 0 ]; // Set first argument as Player Action (pAction)
            
            if( null != pAction )
            {
                switch( pAction )
                {
                    case "begin":
                    case "start":
                    case "ng":
                    case "newgame":
                        if( args.length > 1 )
                        {
                            if( Integer.parseInt( args[1] ) > 2 )
                            {
                                this.Start( Integer.parseInt( args[1] ) );
                                break;
                            }
                            
                            System.out.println("[INFO] The Grid size you have supplied is too small. Defaulting to a minimum of 3.");
                            this.Start( 3 );
                            break;
                        }
                        
                        this.Start( 5 );
                        break;
                    case "d":
                    case "display":
                        String result = (this.currentGame == null) ? "[MAIN] Nothing to display.\n" : this.currentGame.toString();
                        System.out.printf( result );
                        break;
                    case "r":
                    case "reset":
                        this.Reset();
                        break;
                    case "q":
                    case "quit":
                    case "e":
                    case "x":
                    case "exit":
                        System.out.printf( "Thanks for playing! " );
                        break OUTER;
                    case "menu":
                    case "?":
                    case "help":
                        this.DisplayMenu();
                        break;
                    case "v":
                    case "ver":
                    case "vers":
                    case "version":
                        System.out.printf( getVersion() );
                        break;
                    case "ss":
                    case "set":
                    case "setsquare":
                        if( args.length == 4 ) // Check we have enough input arguments
                        {
                            if( args[1] != null && args[2] != null && args[3] != null ) // Check if data is null
                            {
                                if( !args[1].equals("") && !args[2].equals("") && !args[3].equals("") ) // Check if data supplied is empty
                                {
                                    if( ((Integer.parseInt(args[1])-1) >= 0) && ((Integer.parseInt(args[2])-1) >= 0) && (Integer.parseInt(args[3]) >= 0) ) // Check user input is within range expected
                                    {
                                        this.currentGame.setSquare(Integer.parseInt(args[1])-1, Integer.parseInt(args[2])-1, Integer.parseInt(args[3])); // Set square
                                        break;
                                    }
                                }
                            }
                        }
                        System.err.println("Incorrect value supplied! Command use: setsquare [y_coord] [x_coord] [value]");
                        break;
                    case "l":
                    case "legal":
                        String outresult = (this.currentGame.isLegal()) ? "Congratulations! The Puzzle is complete!" : "The Puzzle is not in a legal configuration (incomplete).";
                        System.out.println(outresult);
                        break;
                    default:
                        System.err.println( "COMMAND NOT FOUND!" );
                        break;
                }
            }
            
            // Clear Action (failsafe)
            args = null;
            pAction = null;
        }
    }
}
